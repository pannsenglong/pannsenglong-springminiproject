package springminiproject.demo.models;

public class ResponseMessage {
    private String imageUrl;
    private String message;

    public ResponseMessage() {
    }

    public ResponseMessage(String imageUrl, String message) {
        this.imageUrl = imageUrl;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}

package springminiproject.demo.configurations;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


@Configuration
public class FilesUploadConfig implements WebMvcConfigurer {
    private String client;
    private String server;

    @Value("/image/**")
    public void setClient(String client) {
        this.client = client;
    }

    @Value("uploads/")
    public void setServer(String server) {
        this.server = server;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(client).addResourceLocations("file:"+ server);
    }
}

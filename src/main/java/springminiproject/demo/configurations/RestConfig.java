package springminiproject.demo.configurations;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import springminiproject.demo.entities.Book;
import springminiproject.demo.entities.Category;

@Configuration
public class RestConfig implements RepositoryRestConfigurer
{
    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(Book.class);
        config.exposeIdsFor(Category.class);
        config.setDefaultPageSize(100);
        config.setBasePath("/api/v1");

    }
}

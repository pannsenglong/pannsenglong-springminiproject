package springminiproject.demo.repositories;



import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
import springminiproject.demo.entities.Book;

import java.util.List;

public interface    BookRepositories extends PagingAndSortingRepository<Book, Integer> {

    @RestResource(path = "/category")
    List<Book> findBookByCategoryId(@Param("categoryId") int id);

    @RestResource(path = "/title")
    List<Book> findBookByTitleContainsIgnoreCase(@Param("title") String title);
}

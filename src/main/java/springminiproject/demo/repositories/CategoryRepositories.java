package springminiproject.demo.repositories;


import org.springframework.data.repository.PagingAndSortingRepository;
import springminiproject.demo.entities.Category;

public interface CategoryRepositories extends PagingAndSortingRepository<Category, Integer>
{

}
